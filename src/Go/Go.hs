module Go.Go where

import qualified Data.Vector as V
import Data.List(intercalate)

data Piece = Black | White deriving (Show, Eq)

data GoBoard = GoBoard { boardState :: V.Vector (Maybe Piece) } deriving Eq

goWidth = 19
goHeight = 19

chunkify :: Int -> [a] -> [[a]]
chunkify n = go n where
  go 1 (x:xs) = [x] : go n xs
  go k (x:xs) = let (chunk:chunks) = go (k-1) xs in (x:chunk):chunks
  go _ [] = []

instance Show GoBoard where
  show (GoBoard s) = intercalate "\n" . map concat . chunkify 9 . map showPiece $ (V.toList s) where
    showPiece Nothing = "[ ]"
    showPiece (Just Black) = "[B]"
    showPiece (Just White) = "[W]"

empty :: GoBoard
empty = GoBoard $ V.replicate (goWidth*goHeight) Nothing

data GoPosition = GoPosition !Int !Int deriving Eq

instance Show GoPosition where
  show (GoPosition x y) = "(" ++ show x ++ ", " ++ show y ++ ")"

validPositions :: [GoPosition]
validPositions = GoPosition <$> [0..goWidth] <*> [0..goHeight]

toGoPosition :: Int -> Int -> Maybe GoPosition
toGoPosition x y
  | x >= 0 && x < goWidth && y >= 0 && y < goHeight = Just $ GoPosition x y
  | otherwise = Nothing

toIndex :: GoPosition -> Int
toIndex (GoPosition x y) = x + goWidth * y

isEmptyPos :: GoPosition -> GoBoard -> Bool
isEmptyPos pos board = (boardState board V.! toIndex pos) == None

neighbors :: GoPosition -> [GoPosition]
neighbors (GoPosition x y) = addLeft . addRight . addBottom . addTop $ [] where
  addLeft
    | x > 0 = (GoPosition (x-1) y:)
    | otherwise = id
  addRight
    | x + 1 < goWidth = (GoPosition (x+1) y:)
    | otherwise = id
  addBottom
    | y + 1 < goHeight = (GoPosition x (y+1):)
    | otherwise = id
  addTop
    | y > 0 = (GoPosition x (y-1):)
    | otherwise = id

placePiece :: Piece -> GoPosition -> GoBoard -> Maybe GoBoard
placePiece piece pos board
  | not (isEmptyPos pos board) = Nothing
  | otherwise =
     foldr
       (\neighbor board' -> removeCapturedGroup piece neighbor' board)
       board
       $ neighbors pos
